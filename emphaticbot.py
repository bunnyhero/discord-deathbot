#!/usr/bin/env python
# coding=utf8
# Copyright (c)2017 bunnyhero.  Some rights reserved.
# portions Copyright (c)2009 Sean Langley from https://github.com/slangley/deathbot .  Some rights reserved.
# portions Copyright (C) 2006-2009 Eric Florenzano from the following website:
# http://www.eflorenzano.com/blog/post/writing-markov-chain-irc-bot-twisted-and-python/
#
# see the accompanying LICENSE file

import random
import logging
import configparser
from datetime import datetime
import random
import shlex
import json

import discord
from wordwar import WordWarManager
import botutils

config = configparser.ConfigParser()
config.read("config.ini")


logging.basicConfig(level=logging.INFO, 
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    handlers=[logging.FileHandler('discord.log', 'a', 'utf-8')])


command_help = {
    "!startwar": ("# ## -> create a war lasting # minutes, starting in ## minutes", 0),
    "!throwdown": ("# ## -> create a war lasting # minutes, starting in ## minutes", 1),
    "!status": ("-> list wars that are in progress or not yet started", 2),
    "!joinwar": ("**_@warname_** -> join a word war so you get PM'd on start", 3),
    "!leavewar": ("**_@warname_** -> leave a word war", 4),
    "!time": ("-> what's the server time", 5),
    "!prompt": ("-> display a random writing prompt", 6),
    "!decide": ('"option 1" "option 2" ["option 3"...] -> choose randomly between options', 7),
}


class WordWarBot(discord.Client):

    def __init__(self):
        super().__init__()
        self.wwMgr = WordWarManager(self)
        self.promptarray = []
        self.load_death_and_prompt_arrays()

    def load_death_and_prompt_arrays(self):
        logger = logging.getLogger(__name__)
        self.promptarray.clear()

        logger.info("Reloading Prompt Array")
        with open("promptlist.txt", "r", encoding="utf-8") as f:
            self.promptarray += [line.strip() for line in f.readlines()]
        logger.info("promptarray: \n{}".format(self.promptarray))

    def getRandomPrompt(self):
        prompt = random.choice(self.promptarray)
        return prompt


    async def on_ready(self):
        logger = logging.getLogger(__name__)
        logger.info("Logged in as {}#{} <@{}>".format(self.user.name, self.user.discriminator, self.user.id))
        logger.info('----------------------------')
        self.load()

    async def on_message(self, message):
        logger = logging.getLogger(__name__)

        # we do not want the bot to reply to itself
        if message.author == self.user:
            return

        #father = self.check_for_daddy(user)
        msg = message.content.strip()
        lowmsg = msg.lower()
        user = message.author
        channel = message.channel
        mentions = message.mentions

        # handle !commands
        if lowmsg.startswith('!'):
            command = lowmsg.split(' ')[0]
            if command == "!startwar" or command == "!throwdown":
                await self.parse_startwar(msg, user, channel, command)
            elif command == "!starwar":
                await self.parse_starwars(msg, user, channel)
            elif command == "!joinwar":
                await self.parse_join_wordwar(msg, user, channel, mentions)
            elif command == "!status":
                await self.send_status(user)
            elif command == "!leavewar" or command == "!forfeit" or command == "!surrender":
                await self.parse_leave_wordwar(msg, user, channel, mentions)
            elif command == "!time":
                await self.discord_send_me(channel, "thinks the time is " + datetime.today().strftime('%Y-%m-%d %I:%M:%S %p'))
            elif command == "!help":
                await self.print_usage(user)
            elif command == "!prompt":
                prompt = self.getRandomPrompt()
                # if (self.check_for_daddy(user) == 1):
                #     self.irc_send_say("Yes, father.")
                await self.discord_send_say(channel, "Here's one: " + prompt)
            elif command == "!decide":
                await self.parse_decide(msg, user, channel)
            elif command == "!save":
                self.save()
            elif command == "!load":
                self.load()


    async def parse_starwars(self, msg, user, channel):
        logger = logging.getLogger(__name__)
        logger.info(msg)
        await self.discord_send_say(channel, "{0.mention}: **Star Wars: The Last Jedi** opens Dec 15".format(user))
        await self.discord_send_say(channel, "(perhaps you meant to use **!startwar** ?)")


    async def parse_startwar(self, msg, user, channel, verb_used):
        #logger = logging.getLogger(__name__)

        if self.wwMgr.check_existing_war(user):
            await self.discord_send_msg(user, "Each user can only create one Word War at a time")
            return

        commandlist = [c for c in msg.split(" ") if c != '']
        if (len(commandlist) < 3):
            await self.discord_send_msg(user, "Usage: {} {}".format(verb_used, command_help[verb_used][0]))
            return
        war = await self.initiate_war(user, commandlist, channel)
        if war is not None:
           war.add_user_to_wordwar(user)
           self.save()
           await self.discord_send_msg(user, "You have been added to WW: {0.mention}".format(war.user))

    
    async def initiate_war(self, user, commandlist, channel):
        logger = logging.getLogger(__name__)
        war = self.wwMgr.create_word_war(user, commandlist[1], commandlist[2], self.getRandomPrompt(), channel)
        logger.info("Create word war %s length %s starting in %s", user, commandlist[1], commandlist[2])
        # if (self.check_for_daddy(user) == 1):
        #     self.irc_send_say("Yes father.")
        message = "The gauntlet has been thrown... {0.mention} called a word war of {1}, starting in {2}.".format(
                        user, botutils.minutes_string(commandlist[1]), botutils.minutes_string(commandlist[2]) )
        await self.discord_send_say(channel, message)
        await self.discord_send_say(channel, "Optional Prompt for this WW is: {}".format(war.prompt))
        war.go()

        return war


    async def parse_join_wordwar(self, command, user, channel, mentions):
        # if (self.check_for_daddy(user) == 1):
        #     self.irc_send_say("Yes father.")
        logger = logging.getLogger(__name__)
        logger.info(command)
        commandlist = [c for c in command.split(" ") if c != '']
        war_owner = next(iter(mentions), None)
        if len(commandlist) != 2 or not war_owner:
            await self.discord_send_msg(user, "Usage: {} {}".format(commandlist[0], command_help[commandlist[0]][0]))
            return

        if self.wwMgr.insert_into_war(war_owner, user, channel):
            self.save()
            await self.discord_send_msg(user, "You have been added to WW {0.mention}".format(war_owner))
        else:
            await self.discord_send_msg(user, "There is no word war named {0.mention}".format(war_owner))


    async def parse_leave_wordwar(self, command, user, channel, mentions):
        logger = logging.getLogger(__name__)
        logger.info(command)
        commandlist = [c for c in command.split(" ") if c != '']
        war_owner = next(iter(mentions), None)
        if len(commandlist) != 2 or not war_owner:
            await self.discord_send_msg(user, "Usage: {} {}".format(commandlist[0], command_help[commandlist[0]][0]))
            return

        if self.wwMgr.remove_from_war(war_owner, user, channel):
            self.save()
            await self.discord_send_msg(user, "You have been removed from WW: {0.mention}".format(war_owner))
            if len(self.wwMgr.get_word_war_nicks(war_owner)) == 0:
                await self.wwMgr.cancel_word_war(war_owner)
                self.save()

        else:
            await self.discord_send_msg(user, "You are not part of word war {0.mention}".format(war_owner))


    async def parse_decide(self, msg, user, channel):
        """ Chooses one random option """
        msg = msg.strip()
        choices = shlex.split(msg)
        if len(choices) < 3:
            await self.discord_send_say(channel, "{0.mention}, please provide 2 or more options".format(user))
            return

        del choices[0]
        await self.discord_send_say(channel, "{0.mention}, the dice choose: {1}".format(user, random.choice(choices)))

    async def print_usage(self, user):
        output = ["Bot Usage:"]
        # sort the help items by ordinal
        help_items = sorted(command_help.items(), key=lambda item: item[1][1])
        output += [help[0] + " " + help[1][0] for help in help_items]
        await self.discord_send_msg(user, "\n".join(output))


    async def send_status(self, user):
        statuses = self.wwMgr.get_status()
        for s in statuses:
            await self.discord_send_msg(user, s)

    async def discord_send_say(self, channel, message):
        await self.send_message(channel, message)

    async def discord_send_msg(self, user, message):
        await self.send_message(user, message)

    async def discord_send_me(self, channel, message):
        await self.send_message(channel, "_" + message + "_")


    def save(self):
        logger = logging.getLogger(__name__)
        state = self.wwMgr.save_state()
        savefile = config.get('file', 'savefile', fallback='savefile.txt')
        with open(savefile, 'w', encoding='utf-8') as f:
            json.dump(state, f)
        logger.info("saved to {}".format(savefile))

    def load(self):
        logger = logging.getLogger(__name__)
        savefile = config.get('file', 'savefile', fallback='savefile.txt')
        try:
            with open(savefile, 'r', encoding='utf-8') as f:
                state = json.load(f)
            self.wwMgr.restore_from_state(state)
            logger.info("loaded from {}".format(savefile))
        except Exception as e:
            logger.error("couldn't load: {}".format(e))





logger = logging.getLogger(__name__)
logger.info('----------------- CREATING BOT ---------------')
bot = WordWarBot()

bot.run(config['auth']['token'])

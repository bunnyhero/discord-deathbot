# Copyright (c) 2009-2017, bunnyhero, Sean Langley, and others.
#
# see the accompanying LICENSE file

import asyncio
from datetime import datetime, timedelta
from enum import IntEnum
import logging
import botutils
import discord
import dateutil.parser


class WordWarState(IntEnum):
    WAITING = 0
    RUNNING = 1


class WordWar(object):

    def __init__(self, user, length, start, manager, prompt, channel):
        logger = logging.getLogger(__name__)
        logger.debug("WordWar(%s, %s, %s, %s, %s)" % (user, length, start, prompt, channel))
        self.nicklist = []
        self.user = user
        self.prompt = prompt
        self.length = int(length)
        self.start_delay = int(start)
        self.time_called = datetime.today()
        self.manager = manager
        self.war_start_task = None

        self.status = WordWarState.WAITING
        self.channel = channel


    def save_state(self):
        '''
        Returns a simple jsonable object for persistence
        '''
        # first the simple stuff
        state = {}
        for attr in ['prompt', 'length', 'start_delay']:
            state[attr] = getattr(self, attr)
        
        # enums
        # actually, don't save the status. infer it from time and times
        # state['status'] = self.status.value

        # times
        state['time_called'] = self.time_called.isoformat()

        # discord objects
        state['channel'] = self.channel.id
        state['nicklist'] = [user.id for user in self.nicklist]
        state['user'] = self.user.id

        # extra state that won't be used for restoring, but maybe useful for troubleshooting
        state['debug'] = {'user': self.user.name, 'channel': self.channel.name,
                          'nicklist': [user.name for user in self.nicklist]}
        return state

    def restore_from_state(self, state):
        # first the simple stuff
        for attr in ['prompt', 'length', 'start_delay']:
            setattr(self, attr, state[attr])
        
        # enums
        # actually, don't save the status. infer it from time and times
        # self.status = WordWarState(state['status'])

        # times
        self.time_called = dateutil.parser.parse(state['time_called'])

        # discord objects
        self.channel = discord.utils.get(self.manager.bot.get_all_channels(), id=state['channel'])

        self.nicklist = []
        for user_id in state['nicklist']:
            user = self.channel.server.get_member(user_id)
            if user:
                self.nicklist.append(user)

        self.user = self.channel.server.get_member(state['user'])


    def go(self):
        self._calc_transition_times()
        self.war_start_task = asyncio.ensure_future(self.run_word_war())

    def _calc_transition_times(self):
        self.two_minute_warning_time = (self.time_called + timedelta(minutes=self.start_delay - 2)) if self.start_delay > 2 else None
        self.war_start_time = self.time_called + timedelta(minutes=self.start_delay)
        self.war_end_time = self.war_start_time + timedelta(minutes=self.length)


    async def run_word_war(self):
        logger = logging.getLogger(__name__)

        self.status = WordWarState.WAITING

        if self.two_minute_warning_time is not None and datetime.today() < self.two_minute_warning_time:
            await asyncio.sleep((self.two_minute_warning_time - datetime.today()).total_seconds())
            await self.send_message("WW: {0.mention} starts in 2 minutes for {1}".format(self.user, botutils.minutes_string(self.length)))
            await self.send_message("Optional Prompt for this WW is: {}".format(self.prompt))
            await self.notify_nicks()

        if datetime.today() < self.war_start_time:
            await asyncio.sleep((self.war_start_time - datetime.today()).total_seconds())

            await self.send_message("GOOOOOOOOOOO!!! WW: {0.mention} for {1}".format(self.user, botutils.minutes_string(self.length)))
            await self.send_message("Optional Prompt for this WW is: {}".format(self.prompt))
            await self.notify_nicks()
            logger.info("war {0.mention} started at {1}".format(self.user, self.war_start_time))

        self.status = WordWarState.RUNNING

        if datetime.today() < self.war_end_time:
            await asyncio.sleep((self.war_end_time - datetime.today()).total_seconds())

        logger.info("finish word war, remove from queue")
        await self.send_message("WW: {0.mention} is done - share your results".format(self.user))
        await self.notify_nicks()

        self.manager.done_word_war(self)


    def status_word_war(self):
        status = [
            "name: {0.mention}".format(self.user),
            "length: {}".format(botutils.minutes_string(self.length))
        ]

        if self.status == WordWarState.WAITING:
            # interval = timedelta(minutes=self.start_delay)
            # then = self.time_called + interval
            timeleft = self.war_start_time - datetime.today()
            status += [
                "status: waiting",
                "called at: {}".format(self.time_called.strftime('%Y-%m-%d %I:%M:%S %p')),
                "time until start: {}".format(botutils.format_timedelta(timeleft))
            ]
        else:
            timeleft = self.war_end_time - datetime.today()
            status += [
                "status: underway",
                "started at: {}".format(self.war_start_time.strftime('%Y-%m-%d %I:%M:%S %p')),
                "time until end: {}".format(botutils.format_timedelta(timeleft))
            ]
        status.append("members ({}): {}".format(len(self.nicklist),
                                                ' '.join([nick.mention for nick in self.nicklist])))
        status.append("-----")

        status_str = "\n".join(status)

        return status_str


    async def cancel_word_war(self):
        logger = logging.getLogger(__name__)
        logger.info("cancel word war")
        if self.war_start_task:
            self.war_start_task.cancel()
        self.manager.done_word_war(self)

        await self.send_message("WW: {0.mention} has been cancelled".format(self.user))
        await self.notify_nicks()


    def add_user_to_wordwar(self, user):
        logger = logging.getLogger(__name__)
        logger.info("add_user_to_wordwar(): ww {}, user {}".format(self.user, user))
        self.nicklist.append(user)
        logger.debug("ww {} nicklist now {}".format(self.user, self.nicklist))


    def remove_user_from_wordwar(self, user):
        logger = logging.getLogger(__name__)
        logger.info("remove_user_from_wordwar(): ww {}, user {}".format(self.user, user))
        try:
            self.nicklist.remove(user)
        except ValueError:
            logger.info("{} is not in ww {}'s nicklist".format(user, self.user))
            return False

        logger.debug("ww {}'s nicklist now {}".format(self.user, self.nicklist))
        return True


    async def notify_nicks(self):
        short_nicks = ' '.join([nick.mention for nick in self.nicklist])
        if len(short_nicks):
            await self.manager.discord_send_say(self.channel, "Hey! That means you: {}".format(short_nicks))

    async def send_message(self, message):
        logger = logging.getLogger(__name__)
        logger.info("WordWar.send_message('%s')", message)
        await self.manager.discord_send_say(self.channel, message)
        for nick in self.nicklist:
            await self.manager.discord_send_msg(nick, message)





class WordWarManager(object):
    '''
    The thing that keeps track of the word wars
    '''
    word_wars = []

    def __init__(self, bot):
        self.bot = bot

    def get_war_by_user(self, user):
        return next((war for war in self.word_wars if war.user == user), None)

    def check_existing_war(self, war_owner):
        return self.get_war_by_user(war_owner) != None

    def insert_into_war(self, war_owner, user, channel):
        logger = logging.getLogger(__name__)
        logger.info("insert %s into wordwar '%s'", user, war_owner.mention)
        awar = self.get_war_by_user(war_owner)
        if awar and awar.channel == channel:
            logger.info("Adding %s - %s", awar.user.name, user.name)
            awar.add_user_to_wordwar(user)
            return True

        logger.info("Could not find wordwar '%s', or channel mismatch", war_owner.mention)
        return False

    def remove_from_war(self, war_owner, user, channel):
        logger = logging.getLogger(__name__)
        logger.info("remove %s from wordwar '%s'", user, war_owner.mention)
        awar = self.get_war_by_user(war_owner)
        if awar and awar.channel == channel:
            logger.info("removing %s - %s", awar.user.name, user.name)
            return awar.remove_user_from_wordwar(user)

        logger.info("Could not find wordwar '%s'", war_owner.mention)
        return False


    def create_word_war(self, name, length, start, prompt, channel):
        new_ww = WordWar(name, length, start, self, prompt, channel)
        self.word_wars.append(new_ww)
        return new_ww

    async def cancel_word_war(self, war_owner):
        logger = logging.getLogger(__name__)
        logger.info("cancel wordwar '%s'", war_owner.mention)
        awar = self.get_war_by_user(war_owner)
        if awar:
            await awar.cancel_word_war()
            return True
        logger.info("Could not find wordwar '%s' to cancel", war_owner.mention)
        return False


    def done_word_war(self, wordwar):
        self.word_wars.remove(wordwar)
        self.bot.save()


    def get_word_war_nicks(self, war_owner):
        awar = self.get_war_by_user(war_owner)
        return awar.nicklist if awar else None


    def get_status(self):
#        if (self.check_for_daddy(user) == 1):
#                self.bot_send_say("Yes father.");
        if len(self.word_wars) == 0:
            return ["There are no active word wars"]

        return [ww.status_word_war() for ww in self.word_wars]


    def save_state(self):
        war_states = [war.save_state() for war in self.word_wars]
        return war_states


    def restore_from_state(self, state):
        # kill all existing wars! don't use the usual methods because we don't want to notify anyone
        # in practice this isn't going to happen, because restoring normally only happens on startup?
        for war in self.word_wars:
            if war.war_start_task:
                war.war_start_task.cancel()

        wars = []
        for war_state in state:
            # i'm lazy, so instead of rewriting the initializer, i'm making new wars with dummy values first...
            new_ww = WordWar("", 0, 0, self, "", None)
            new_ww.restore_from_state(war_state)
            wars.append(new_ww)

        self.word_wars = wars

        # oh yeah restart them all?
        for war in self.word_wars:
            war.go()


    # def irc_send_me(self, message):
    #     await self.bot.irc_send_me(message)

    async def discord_send_say(self, channel, message):
        await self.bot.discord_send_say(channel, message)

    async def discord_send_msg(self, user, message):
        await self.bot.discord_send_msg(user, message)

    @property
    def loop(self):
        return self.bot.loop

